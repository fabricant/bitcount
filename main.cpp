#include <iostream>
#include <ctime>
#include <map>
#include <cstring>

using namespace std;
const size_t valuesCount = 1000000;

struct BitStruct{
    union{
        struct{
            uint8_t x0:1;
            uint8_t x1:1;
            uint8_t x2:1;
            uint8_t x3:1;
            uint8_t x4:1;
            uint8_t x5:1;
            uint8_t x6:1;
            uint8_t x7:1;
        };
        uint8_t x;
    };

    inline uint8_t getBitCount() {
        return x0 + x1 + x2 + x3 + x4 + x5 + x6 + x7;
    }
};

struct StopWatch {
    std::clock_t start;
    double duration;
    StopWatch() {
        start = std::clock();

    }

    ~StopWatch() {
        duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

        std::cout<<"time: "<< duration <<'\n';
    }
};

inline uint8_t getBitCount(uint8_t &x);
inline uint8_t getBitCount2(uint8_t &x);
inline uint8_t getBitCount3(uint8_t &x);
uint8_t bitMap[255];

int main()
{
    BitStruct bs;

    for  (size_t  i = 0; i <= 255; ++i){
        memcpy(&bs, &i, 1);
        bitMap[i] = bs.getBitCount();
    }

    uint8_t x = 5;
    size_t count = getBitCount(x);

    cout  << count << endl;

    x = 10;
    count = getBitCount(x);
    cout  << count << endl;

    x = 7;
    count = getBitCount(x);
    cout  << count << endl;

    srand(time(nullptr));
    uint8_t *randomValues = new uint8_t[valuesCount];

    for (size_t i = 0; i < valuesCount; ++i) {
        randomValues[i] = rand()%255;
    }



    {
        StopWatch stopWatch;
        for (size_t i = 0; i < valuesCount; ++i) {
            getBitCount(randomValues[i]);

        }
    }

    {
        StopWatch stopWatch;
        for (size_t i = 0; i < valuesCount; ++i) {
            getBitCount2(randomValues[i]);
        }
    }

    {
        StopWatch stopWatch;
        for (size_t i = 0; i < valuesCount; ++i) {
            getBitCount3(randomValues[i]);
        }
    }
    return 0;
}




inline uint8_t getBitCount(uint8_t &x) {
    BitStruct b{0};
    b.x = x;
    return b.getBitCount();
}

inline uint8_t getBitCount2(uint8_t &x) {
    return bitMap[x];
}

inline uint8_t getBitCount3(uint8_t &x) {
    uint8_t count = 0;
    while (x) {
        count += x&0x1u;
        x = x>>1;
    }
    return count;
}
